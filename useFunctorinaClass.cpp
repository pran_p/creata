#include "stdafx.h"
#include <Windows.h>
#include <thread>
#include <vector>
#include <chrono>
#include <iostream>
#include <mutex>
class resampler;
typedef void (resampler::*FunPtr)(int p);
	class resampler {
	public:
		FunPtr func;

		void myfunc(int p)
		{
			printf("hello");
		}
		
		void calling()
		{
			(this->*func)(10);
		}
	};

	int main()
	{
		resampler p;
		p.func = &resampler::myfunc;
		printf("%p", &resampler::myfunc);
		resampler *ptr;
		ptr = &p;
		ptr->calling();
	}
