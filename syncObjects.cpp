#include "stdafx.h"
#include <windows.h>
#include <iostream>
#include <string>
#include <memory>

class autocriticalsection {
	private:
		CRITICAL_SECTION m_cs;
	public:
		autocriticalsection() { InitializeCriticalSection(&m_cs); }
		~autocriticalsection() { DeleteCriticalSection(&m_cs); }
		operator CRITICAL_SECTION * () { return &m_cs; }
	
	};

class automutex {
    private:
		autocriticalsection m_cs;
	public:
		automutex(autocriticalsection &cs)
		{
			m_cs = cs;
			if (m_cs)
			{
				EnterCriticalSection(m_cs);
			}
		}

		~automutex()
		{
			if (m_cs)
			{
				LeaveCriticalSection(m_cs);

			}
		}
};

	
int main()
{		
		autocriticalsection mutex1;
		CRITICAL_SECTION *p = (mutex1);
		automutex m(mutex1);
}

